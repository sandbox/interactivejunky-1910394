This module exposes a new field group type called if/then for any field group
compatible entity.

USAGE
1. Create an if/then field group for the entity on the field configuration page.
2. Put any fields you wish to hide by default within the if/then field group.
3. In the field group condition, set the action that will show the field group.

EXAMPLE USAGE
* Only show 'other' textarea if the 'other' option is selected in radio buttons.

By Marton Bodonyi - for FidgeFriend.