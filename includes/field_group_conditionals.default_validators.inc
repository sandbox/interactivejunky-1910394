<?php

/**
 * @file
 * Default conditional validators for the Conditional Field Groups module.
 */

/**
 * Determines if the provided field value is equal to another value.
 * @param	string	$field_value The user selected value on the current form.
 * @param	string	$set_value	The value the field is compared against.
 * @return	bool  The result of the validation.
 */
function field_group_conditionals_condition_match($field_value, $set_value, $form_state = array()) {
  return $field_value == $set_value;
}

/**
 * Determines if the provided field value is greater than another value.
 * @param	string	$field_value The user selected value on the current form.
 * @param	string	$set_value	The value the field is compared against.
 * @return	bool  The result of the validation.
 */
function field_group_conditionals_condition_greater($field_value, $set_value, $form_state = array()) {
  return $field_value > $set_value;
}

/**
 * Determines if the provided field value is less than another value.
 * @param	string	$field_value The user selected value on the current form.
 * @param	string	$set_value	The value the field is compared against.
 * @return	bool  The result of the validation.
 */
function field_group_conditionals_condition_less($field_value, $set_value, $form_state = array()) {
  return $field_value < $set_value;
}
