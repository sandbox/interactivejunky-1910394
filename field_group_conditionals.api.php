<?php

/**
 * @file
 * Hooks provided by Conditional Fieldgroups
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Inform field group conditionals about the conditional functions available.
 *
 * @return array
 *  An array of conditional functions available to the field group formatter.
 */
function hook_field_group_conditionals_types() {
  $module_path = drupal_get_path('module', 'field_group_conditionals');

  return array(
    'match' => array(
      'title' => 'is equal to',
      'client_side' => array(
        'validation' => 'fieldGroupConditionalsConditionMatch',
        'file' => array(
          'module' => 'field_group_conditionals',
          'path' => 'field_group_conditionals.clientside.js',
        ),
      ),
      'server_side' => array(
        'validation' => 'field_group_conditionals_condition_match',
        'file' => array(
          'module' => 'field_group_conditionals',
          'path' => 'includes/field_group_conditionals.default_validators',
          'type' => 'inc',
        ),
      ),
    ),
  );
}
/**
 * @} End of "addtogroup hooks".
 */
