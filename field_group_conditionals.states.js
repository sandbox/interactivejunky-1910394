/**
 * @file
 * Custom States API validation functions for Conditional Field Groups module.
 *
 * Other modules can create additional states using the Conditional Field Groups
 * API.
 */

Drupal.fieldGroupConditionals = Drupal.fieldGroupConditionals ? Drupal.fieldGroupConditionals : {};
/**
 * Checks to see if the field value is greater than the test value.
 * @param  {string} testValue  The value to test against.
 * @param  {string} fieldValue The field value to test.
 * @param  {object} $element   The jQuery field object.
 * @return {boolean}           The result of the comparison.
 */
Drupal.fieldGroupConditionals.fieldGroupGreaterThan = function(testValue, fieldValue, $element) {
  return Number(fieldValue) > Number(testValue);
}
/**
 * Checks to see if the field value is less than the test value.
 * @param  {string} testValue  The value to test against.
 * @param  {string} fieldValue The field value to test.
 * @param  {object} $element   The jQuery field object.
 * @return {boolean}           The result of the comparison.
 */
Drupal.fieldGroupConditionals.fieldGroupLessThan = function(testValue, fieldValue, $element) {
  return Number(fieldValue) < Number(testValue);
}
/**
 * Checks to see if the field value is equal to the test value.
 * @param  {string} testValue  The value to test against.
 * @param  {string} fieldValue The field value to test.
 * @param  {object} $element   The jQuery field object.
 * @return {boolean}           The result of the comparison.
 */
Drupal.fieldGroupConditionals.fieldGroupMatch = function(testValue, fieldValue, $element) {
  return fieldValue == testValue;
}
