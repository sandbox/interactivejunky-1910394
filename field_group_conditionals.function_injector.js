/**
 * @file
 * States API fieldGroupFunction state handler.
 *
 * Hooks boolean validation functions in States API and allows the use of custom
 * validation functions.
 *
 * Adapted from the States API change function but returns a function call
 * instead of the value of the field.
 */
(function ($) {

Drupal.states.Trigger.states.fieldGroupFunction = {
  'keyup': function () {
    var value = this.val(),
        functionSettings = Drupal.settings.fieldGroupConditionals[this.selector],
        fn = Drupal.fieldGroupConditionals[functionSettings.functionName],
        testValue = functionSettings.testValue;

    if (this.attr('type') == 'checkbox' || this.attr('type') == 'radio') {
      value = this.filter(':checked').val() || 0;
    }

    return fn(testValue, value, this);

  },
  'change': function () {
    var value = this.val(),
        functionSettings = Drupal.settings.fieldGroupConditionals[this.selector],
        fn = Drupal.fieldGroupConditionals[functionSettings.functionName],
        testValue = functionSettings.testValue;

    if (this.attr('type') == 'checkbox' || this.attr('type') == 'radio') {
      value = this.filter(':checked').val() || 0;
    }

    return fn(testValue, value, this);
  }
}
})(jQuery);
